describe('Tickets', () => {

    beforeEach(() => {
        cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html')
    })

    it('Fills all the text inputs fields', () => {
        const firstName = 'Hamon';
        const lastName = 'Córdova';
        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('hamoncj@gmail.com')
        cy.get('#requests').type('Vegetarian')
        cy.get('#signature').type(`${firstName} ${lastName}`)
    })

    it('Select two tickets', () => {
        cy.get('#ticket-quantity').select('2')
    })

    it('Select vip ticket type', () => {
        cy.get('#vip').check()
    })

    it('Selects Social media checkbox', () => {
        cy.get('#social-media').check()
    })


    it("Selects 'friend' and 'publication' and uncheck friend", () => {
        cy.get('#friend').check()
        cy.get('#publication').check()
        cy.get('#friend').uncheck()
    })

    it("Make email input invalid when type a invalid email", () => {

        cy.get('#email').as('emailInput').type('invalidemail');
        cy.get('#email.invalid').should('exist')

        cy.get('@emailInput').clear();

        cy.get('#email invalid').should('not.exist');
    })

    it("fills and reset the form", () => {
        const firstName = 'Hamon';
        const lastName = 'Córdova';
        const fullName = `${firstName} ${lastName}`;
        const tickets = '2';

        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('hamoncj@gmail.com')
        cy.get('#requests').type('Vegetarian')
        cy.get('#ticket-quantity').select(tickets)
        cy.get('#vip').check()
        cy.get('#friend').check()

        cy.get('.agreement p').should('contain', `I, ${fullName}, wish to buy ${tickets} VIP tickets`)

        cy.get('#agree').click();
        cy.get('#signature').type(`${fullName}`)

        cy.get('button[type=submit]').as('submitButton').should('not.be.disabled');
        cy.get('button[type=reset]').click();
        cy.get('@submitButton').should('be.disabled');

    })

    it("fills mandatory fields using support command", () => {
        const costumer = {
            firstName: 'João',
            lastName: 'Silva',
            email: 'joaosilva@example.com',
        }

        cy.fillMandatoryFields(costumer);

        cy.get('button[type=submit]').as('submitButton').should('not.be.disabled');
        cy.get('#agree').uncheck();
        cy.get('@submitButton').should('be.disabled');

    })


})

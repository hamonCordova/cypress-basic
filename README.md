## Cypress - Basic

This repository was create to test a fake website, using [Testes automatizados com Cypress básico](https://www.udemy.com/course/testes-automatizados-com-cypress-basico) course as base to learn and apply all this codes.

[![pipeline status](https://gitlab.com/hamonCordova/cypress-basic/badges/main/pipeline.svg)](https://gitlab.com/hamonCordova/cypress-basic/-/commits/main)
